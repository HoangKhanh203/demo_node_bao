const express = require('express');

const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
require('dotenv').config();

const mockData = require('./mockData.json');

app.post("/account/signin", (req,res,next) => {

    const paramsBody = req.body;

    const username = paramsBody.username || '';
    const password = paramsBody.password || '';
    
    // KHÚC NÀY là query sql
    const resSQL = mockData.account.find(item => {
        return item.username === username && item.password === password
    })
    // KHÚC NÀY là query sql

    if(!resSQL){ // => nghĩa là bị undefinde
        res.send({
            status: "ERROR",
            error: {
                error_code: "USER_001",
                message: "tài khoản và mật khẩu không tồn tại"
            },
            message: "Đăng nhập thất bại"
        })
    }else {
        res.send({
            status: "OK",
            error: null,
            message: "Đăng nhập thành coong"
        })
    }
});


app.listen(process.env.POST || 5000,() => {
    console.log("server is running")
})